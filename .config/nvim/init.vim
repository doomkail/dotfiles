let mapleader =","

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
"Themes
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tomasiser/vim-code-dark'
" Working with tags
Plug 'tpope/vim-surround'
Plug 'alvan/vim-closetag'
"Commentary
Plug 'tpope/vim-commentary'
""Code completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"vim-visual-multi
Plug 'mg979/vim-visual-multi'
" Syntax highlighting
Plug 'ap/vim-css-color'
" Quickscope
Plug 'unblevable/quick-scope'
"NerdTree
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
" Icons in the file explorer and file type status line
Plug 'ryanoasis/vim-devicons'
Plug 'jreybert/vimagit'
" EmmetVim
Plug 'mattn/emmet-vim'                             "Web auto-complete
" Search results counter
Plug 'vim-scripts/IndexedSearch'
" Async autocompletion
Plug 'Shougo/deoplete.nvim'
" Git integration
Plug 'tpope/vim-fugitive'
 
call plug#end()

let g:airline_theme = 'codedark'
        " Some basics:
	nnoremap c "_c
	set nocompatible
	filetype plugin indent on
	syntax on
	set encoding=utf-8
	set number relativenumber
        set mouse=a
	set termguicolors
	colorscheme codedark
	set expandtab
	set smartcase
        set guifont=Inconsolata\ 12
        "Use system clipboard
	set clipboard+=unnamedplus
        " Enable autocompletion:
	set wildmode=longest,list,full
        "" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
	set splitbelow splitright

" Nerd tree
	map <leader>n :NERDTreeToggle<CR>
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

" Better window navigation
        nnoremap <C-h> <C-w>h
        nnoremap <C-j> <C-w>j
        nnoremap <C-k> <C-w>k
        nnoremap <C-l> <C-w>l
" Alternate way to save
  nnoremap <silent> <C-s> :w<CR>
" Alternate way to quit
  nnoremap <silent> <C-Q> :q!<CR>
" Use control-c instead of escape
  nnoremap <silent> <C-c> <Esc>
" <TAB>: completion.
  inoremap <silent> <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
"Enable spacebar folding  Normal mode
	nnoremap <space> za

"" Open corresponding .pdf/.html or preview
	map <leader>p :!opout <c-r>%<CR><CR>

" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!
"" When shortcut files are updated, renew bash and ranger configs with new material:
"	autocmd BufWritePost files,directories !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
	autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
" Vim Quickscope
" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline

" Set persistent undo config
" Use persistent history.
if !isdirectory("/tmp/.vim-undo-dir")
    call mkdir("/tmp/.vim-undo-dir", "", 0700)
endif
set undodir=/tmp/.vim-undo-dir
set undofile

"compiles with sudo dwmblocks"
autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
