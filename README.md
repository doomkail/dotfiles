# Doomkail's Dotfiles

Estos archivos son los archivos de configuracion

- Algunos scripts utiles estan en `~/.local/bin/`
- Configuraciones para:
	- vim/nvim (editor de texto)
	- mpd/ncmpcpp (musica)
    - sxiv (visor de imagenes/gif)
	- mpv (reproductor de video)
    - otras configuraciones como alias, xdg y mas.
- Se intenta minimizar los archivos que estan directamente en `~`:
	- Todas las configuraciones deberian estar `~/.config/`
    
## Usos

Estos archivos estan pensados en usar programas suckless que suelo usar:

- [dwm] (https://gitlab.com/doomkail/dwm) (window manager)
- [dwmblocks] (barra de tareas)
- [st] (emulador de terminal)
